<?php

class MR_Cachebuster_Model_Observer
{

    public function controller_action_postdispatch(Varien_Event_Observer $observer)
    {

        $helper = Mage::helper('mr_cachebuster');
        if (!$helper->isEnabled()) {
            return;
        }
        $parser   = $helper->getParser();
        $response = $observer->getControllerAction()->getResponse();
        $body     = $parser->parseHtml($response->getBody());
        $response->setBody($body);
    }
}
